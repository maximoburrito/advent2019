(ns advent2019.day02
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

(defn parse-program [input]
  (mapv parse-long (str/split (str/trim-newline input) #",")))

(def full-input
  (parse-program (slurp (io/resource "input02.txt"))))

(defn run1 [program]
  (loop [pc 0 mem program]
    (let [instruction (mem pc)]
      (cond
        (= 99 instruction)
        (mem 0)

        (= 1 instruction)
        (let [a (mem (+ pc 1))
              b (mem (+ pc 2))
              c (mem (+ pc 3))]
          (recur (+ 4 pc)
                 (assoc mem c (+ (mem a) (mem b)))))

        (= 2 instruction)
        (let [a (mem (+ pc 1))
              b (mem (+ pc 2))
              c (mem (+ pc 3))]
          (recur (+ 4 pc)
                 (assoc mem c (* (mem a) (mem b)))))

        :else
        :error))))


(defn patch [input noun verb]
  (-> input
      (assoc 1 noun)
      (assoc 2 verb)))

(defn part1 [input]
  (run1 (patch input 12 2)))

(defn part2 [input]
  (first
   (for [noun (range 100)
         verb (range 100)
         :when (= 19690720 (run1 (patch input noun verb)))]
     (+ verb (* 100 noun)))))


(comment
  (= 3500 (run1 (parse-program "1,9,10,3,2,3,11,0,99,30,40,50")))
  (= 30 (run1 (parse-program "1,1,1,4,99,5,6,0,99")))
  (= 3790689 (part1 full-input))

  (= 6533 (part2 full-input)))
