(ns advent2019.day01
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(defn fuel [mass]
  (- (int (/ mass 3)) 2))


(defn sum [nums]
  (reduce + nums))

(defn parse-input [text]
  (for [line (str/split-lines text)]
    (parse-long line)))

(def full-input
  (parse-input (slurp (io/resource "input01.txt"))))

;; ----------------------------------------

(defn part1 [nums]
  (sum (map fuel nums)))


;;----------------------------------------

(defn fuel2 [mass]
  (sum (take-while pos? (drop 1 (iterate fuel mass)))))

(defn part2 [nums]
  (sum (map fuel2 nums)))

;; ----------------------------------------

(comment
  (= 2 (fuel 12))
  (= 2 (fuel 14))
  (= 33583 (fuel 100756))
  (= 654 (fuel 1969))
  (= 3289802 (part1 full-input))

  (= 4931831 (part2 full-input)))
