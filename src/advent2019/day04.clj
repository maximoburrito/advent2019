(ns advent2019.day04
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.set :as set]))


(defn parse-input [text]
  (mapv parse-long (str/split text #"-")))

(def full-input
  (parse-input "278384-824795"))

(defn ascending? [pairs]
  ;; pairs are chars
  (every? (fn [[a b]]
            (<= (int a) (int b)))
          pairs))

(defn repeat? [pairs]
  (some (fn [[a b]]
          (= a b))
        pairs))


(defn part1 [input]
  (let [[from to] input]
    (count
      (for [n (range from (inc to))
            :let [pairs (partition 2 1 (str n))]

            :when (and (ascending? pairs)
                       (repeat? pairs))]
        n))))


(defn repeat2? [n]
  ;; group-by identity to create pairs of [digit digit-spans]
  ;; then look if there is a digit whose span is 2
  (some (fn [[n ns]]
           (= 2 (count ns)))
        (group-by identity (str n))))

(defn part2 [input]
  (let [[from to] input]
    (count
      (for [n (range from (inc to))
            :when (and (ascending? (partition 2 1 (str n)))
                       (repeat2? n))]
        n))))


(comment
  (= 921 (part1 full-input))

  (= 602 (part2 full-input)))
