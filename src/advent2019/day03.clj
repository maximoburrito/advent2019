(ns advent2019.day03
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.set :as set]))

(defn split-comma [text]
  (str/split text #","))

(defn parse-command [cmd]
  [(subs cmd 0 1)
   (parse-long (subs cmd 1))])

(defn parse-input [text]
  (for [command-list (str/split-lines text)]
    (for [command (split-comma command-list)]
      (parse-command command))))

(def sample1
  (parse-input (slurp (io/resource "sample03-1.txt"))))

(def full-input
  (parse-input (slurp (io/resource "input03.txt"))))

(def dirs
  {"L" [-1 0]
   "R" [1 0]
   "U" [0 1]
   "D" [0 -1]})

(defn move [[x y] [dx dy]]
  [(+ x dx) (+ y dy)])

(defn run [commands]
  (loop [commands commands
         n-steps 1
         pos [0 0]
         steps {}]
    (Thread/sleep 1)
    (if (empty? commands)
      steps
      (let [[dir n] (first commands)
            delta (dirs dir)
            points (drop 1 (take (inc n)
                                 (iterate #(move % delta)
                                          pos)))
            new-steps (into {}
                            (map vector points (range n-steps Integer/MAX_VALUE)))]
        (recur (rest commands)
               (+ n-steps n)
               (last points)
               (merge-with min steps new-steps))))))

(defn manh [[x y]]
  (+ (abs x)
     (abs y)))

(defn part1 [wires]
  (let [[wire1 wire2] wires
        crosses (set/intersection (set (keys (run wire1)))
                                  (set (keys (run wire2))))]
    (reduce min (map manh crosses))))




(defn part2 [wires]
  (let [[wire1 wire2] wires
        res1 (run wire1)
        res2 (run wire2)
        crosses (set/intersection (set (keys res1))
                                  (set (keys res2)))]
    (apply min
           (for [cross crosses]
             (+ (res1 cross)
                (res2 cross))))))



(comment
  (= 6 (part1 sample1))
  (= 260 (part1 full-input))

  (= 30 (part2 sample1))
  (= 15612 (part2 full-input)))
